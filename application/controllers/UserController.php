<?php

use Model\User;
use Views\Scripts\Form\Userform;

class UserController extends Zend_Controller_Action{

	/*public function init(){
      Zend_Loader::loadClass ('Model_User'); 
    }*/

    /*public function indexAction(){
        $u = new User();
        $this->view->user = $u->listall();


        $us = new User();
        $this->view->user2 = $us->listall2();
    }*/


    public function indexAction()
    {
        $muser = new User();
        $paginator = Zend_Paginator::factory($muser->listall2());
        $paginator->setItemCountPerPage(3);
        $paginator->setPageRange(3);
        $currentPage = $this->_request->getParam('page',1);
        $paginator->setCurrentPageNumber($currentPage);
        $this->view->data=$paginator;



        $form = new Form_User();

        $this->view->form=$form;
    }

}
