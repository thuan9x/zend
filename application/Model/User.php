<?php
namespace Model;

class User extends \Zend_Db_Table_Abstract{
    protected $_name= "user";
    protected $_primary="id";
    protected $db;
    public function listall(){
        $data=$this->select();
        $data=$this->fetchAll($data);
        return $data;
    }

    public function listall2(){
        $sql=$this->select();
        $sql->from('user',array('id','username','password','level'));
        $sql=$this->fetchAll($sql);
        return $sql;
    }
}