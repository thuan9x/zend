<?php
define('ROOT_DIR', realpath(dirname(dirname(__FILE__))));

define('APPLICATION_PATH', ROOT_DIR . '/application');

defined('APPLICATION_ENV') 
    || define('APPLICATION_ENV', 
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') 
                                         : 'production'));

set_include_path(implode(PATH_SEPARATOR, array(ROOT_DIR . '/library',get_include_path(),)));

require_once 'Zend/Application.php' ;

$application = new Zend_Application( 
    APPLICATION_ENV, 
    APPLICATION_PATH . '/configs/application.ini'
);

spl_autoload_register(function($class) {
    $class = str_replace('\\', '/', $class);
    require_once APPLICATION_PATH . '/' . $class . '.php';
});


$application->bootstrap()->run();